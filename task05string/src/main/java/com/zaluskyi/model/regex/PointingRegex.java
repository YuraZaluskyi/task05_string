package com.zaluskyi.model.regex;

public class PointingRegex {
    public static String deleteTabsSpaces(String string) {
        return string.replaceAll("\\t| +", " ");
    }
}
