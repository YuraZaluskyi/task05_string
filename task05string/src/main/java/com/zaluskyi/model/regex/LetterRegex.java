package com.zaluskyi.model.regex;

import java.util.List;
import java.util.regex.Pattern;

public class LetterRegex {
    public static boolean isVowel(char letter) {
        return Pattern.compile("[aeiouAEIOU]").matcher(String.valueOf(letter)).find();
    }

    public static boolean isConsonant(String letter) {

        return Pattern.compile("[^aeiouAEIOU]").matcher(letter).find();
    }

    public static List<String> getConsonant(String string) {

        return CustomerPattern.assemble("[^aeiouAEIOU]", string);
    }

    public static List<String> find(String string, char ch) {
        String insert = "[" + Character.toUpperCase(ch) + ch + "]";
        return CustomerPattern.assemble(insert, string);
    }


}
