package com.zaluskyi.model.regex;

import java.util.regex.Pattern;
import java.util.List;
import java.util.Optional;
import java.util.Comparator;

public class SentenceRegex {
    public static List<String> getAllSentence(String string) {
        return CustomerPattern.assemble("[^.?!]+[.?!]", string);
    }

    public static List<String> getQuestion(String string) {
        return CustomerPattern.assemble("[^.?!]+[?]", string);
    }

    public static boolean isSentence(String string) {
        return Pattern.compile("[A-Z][^.?!]+[.?!]").matcher(string).find();
    }

    public static String[] splitSentenceBytheOrYou(String string) {
        return string.split("the|you");
    }

    public static String replaceVowelToUnderscore(String string) {
        StringBuilder stringBuilder = new StringBuilder();
        for (char ch : string.toCharArray()) {
            if (LetterRegex.isVowel(ch)) {
                stringBuilder.append("_");
            } else {
                stringBuilder.append(ch);
            }
        }
        return stringBuilder.toString();
    }

    public static Optional<String> getWordMaxLength(String string) {
        return WordRegex.stringStream(string)
                .sorted(Comparator.comparing(String::length, Comparator.reverseOrder()))
                .findFirst();
    }

    public static List<String> findMaxPartSentence(String string, char start, char end) {
        String insert = "[^’']\\b[" + start + "].+[" + end + "]\\b";
        return CustomerPattern.assemble(insert, string);
    }


}
