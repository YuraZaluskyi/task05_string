package com.zaluskyi.model.regex;

import java.util.stream.Stream;

public class WordRegex {
    public static Stream<String> stringStream(String string) {
        return CustomerPattern.assemble("\\w+’*'*\\w*", string).stream();
    }

    public static Stream<String> specificStream(String string) {
        return CustomerPattern.assemble("(\\w+’*'*\\w*\\b)(?!.*\\1\\b)", string).stream();
    }

    public static Stream<String> vowel(String string) {
        return CustomerPattern.assemble("(\\b[aeiouAEIOU]\\w*)", string).stream();
    }
}
