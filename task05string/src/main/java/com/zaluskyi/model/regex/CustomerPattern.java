package com.zaluskyi.model.regex;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomerPattern {
    public static List<String> assemble(String string, String text) {
        final Matcher matcher = Pattern.compile(string).matcher(text);
        List<String> word = new LinkedList<>();
        while (matcher.find()) {
            word.add(text.substring(matcher.start(), matcher.end()).trim());
        }
        return word;
    }
}
