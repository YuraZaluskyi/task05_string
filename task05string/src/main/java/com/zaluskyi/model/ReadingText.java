package com.zaluskyi.model;

import java.nio.charset.StandardCharsets;

import java.io.*;

public class ReadingText {
    public static String getText(String path) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        File file = new File(path);
        try (BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(file), StandardCharsets.UTF_8.name()))) {
            String string;
            while ((string = bufferedReader.readLine()) != null) {
                stringBuilder.append("\t" + string);
            }
        }
        return stringBuilder.toString();
    }
}
