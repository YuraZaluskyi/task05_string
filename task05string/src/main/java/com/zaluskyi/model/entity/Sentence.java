package com.zaluskyi.model.entity;

public class Sentence {
    private String sentence;
    private String word;
    private long quantityWord;

    public Sentence() {
    }

    public Sentence(String sentence, String word, long quantityWord) {
        this.sentence = sentence;
        this.word = word;
        this.quantityWord = quantityWord;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public long getQuantityWord() {
        return quantityWord;
    }

    public void setQuantityWord(long quantityWord) {
        this.quantityWord = quantityWord;
    }
}
