package com.zaluskyi.model.entity;

public class Word {
    private String word;
    private long quantity;
    private double quantityDouble;

    public Word(String word, long quantity) {
        this.word = word;
        this.quantity = quantity;
    }

    public Word(String word, double quantityDouble) {
        this.word = word;
        this.quantityDouble = quantityDouble;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public double getQuantityDouble() {
        return quantityDouble;
    }

    public void setQuantityDouble(double quantityDouble) {
        this.quantityDouble = quantityDouble;
    }

    @Override
    public String toString() {
        return "WordRegex{" +
                "word='" + word + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
