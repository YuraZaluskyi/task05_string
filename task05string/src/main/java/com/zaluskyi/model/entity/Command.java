package com.zaluskyi.model.entity;

import com.zaluskyi.controller.Controller;

public class Command {
    private String title;
    private Controller controller;

    public Command(String title, Controller command) {
        this.title = title;
        this.controller = command;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Controller getController() {
        return controller;
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }
}
