package com.zaluskyi.model.entity;

import java.util.Locale;

public class Menu {
    private String commandTitle;
    private Locale locale;

    public Menu(String commandTitle, Locale locale) {
        this.commandTitle = commandTitle;
        this.locale = locale;
    }

    public String getCommandTitle() {
        return commandTitle;
    }

    public void setCommandTitle(String commandTitle) {
        this.commandTitle = commandTitle;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }
}
