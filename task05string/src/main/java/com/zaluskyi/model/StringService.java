package com.zaluskyi.model;

public class StringService<T> {
    public String concatenate(T... words) {
        StringBuilder stringBuilder = new StringBuilder();
        for (T word : words) {
            stringBuilder.append(word.toString());
        }
        return stringBuilder.toString();
    }

}
