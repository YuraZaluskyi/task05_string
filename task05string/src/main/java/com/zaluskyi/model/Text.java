package com.zaluskyi.model;

import com.zaluskyi.model.regex.PointingRegex;

import java.io.IOException;
import java.util.Optional;

public class Text {
    public static Optional<String> getReformedText(String path) {
        try {
            return Optional.of(PointingRegex.deleteTabsSpaces(ReadingText.getText(path)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
