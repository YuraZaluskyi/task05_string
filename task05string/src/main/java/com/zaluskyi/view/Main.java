package com.zaluskyi.view;

import com.zaluskyi.controller.MainMenuController;

public class Main {
    public static void main(String[] args) {
        new MainMenuController().start();
    }
}
