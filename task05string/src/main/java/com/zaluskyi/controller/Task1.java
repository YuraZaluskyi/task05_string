package com.zaluskyi.controller;

import com.zaluskyi.model.Text;
import com.zaluskyi.model.regex.SentenceRegex;
import com.zaluskyi.model.regex.WordRegex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class Task1 implements Controller {
    private static Logger logger = LogManager.getLogger(Task1.class);

    @Override
    public void run() {
        String text = Text.getReformedText("textfile.txt").orElse("file not exist or empty");
        final Optional<Map.Entry<String, Long>> stringLongEntry = maxQuantityWord(wordQuantity(text));
        logger.info(stringLongEntry.get().getKey() + " " + quantitySentence(text, stringLongEntry.get().getKey()));
    }

    private Map<String, Long> wordQuantity(String string) {
        return WordRegex.stringStream(string)
                .map(String::toLowerCase)
                .collect(Collectors.groupingBy(word -> word, Collectors.counting()));
    }

    private Optional<Map.Entry<String, Long>> maxQuantityWord(Map<String, Long> map) {
        return map.entrySet()
                .stream()
                .max(Comparator.comparing(Map.Entry::getValue));
    }

    private long quantitySentence(String string, final String word) {
        return SentenceRegex.getAllSentence(string).stream()
                .filter(s -> s.toLowerCase().contains(word))
                .count();
    }
}
