package com.zaluskyi.controller;

import com.zaluskyi.model.Text;
import com.zaluskyi.model.regex.SentenceRegex;
import com.zaluskyi.model.regex.WordRegex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

public class Task3 implements Controller {
    private static Logger logger = LogManager.getLogger(Task3.class);

    public void run() {
        String text = Text.getReformedText("textfile.txt").orElse("file not exist or empty");
        logger.info(specificWordFromFirstSentence(text).get());
    }

    private Optional<String> specificWordFromFirstSentence(String string) {
        return SentenceRegex.getAllSentence(string).stream()
                .limit(1)
                .flatMap(WordRegex::stringStream)
                .filter(word -> SentenceRegex.getAllSentence(string).stream()
                        .skip(1)
                        .noneMatch(s -> s.contains(word)))
                .findFirst();
    }
}
