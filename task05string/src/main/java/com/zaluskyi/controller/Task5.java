package com.zaluskyi.controller;


import com.zaluskyi.model.Text;
import com.zaluskyi.model.regex.SentenceRegex;
import com.zaluskyi.model.regex.WordRegex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;

public class Task5 implements Controller {
    private static Logger logger = LogManager.getLogger(Task5.class);

    public void run() {
        String text = Text.getReformedText("textfile.txt").orElse("file not exist or empty");
        List<String> list = shiftVowelMaxLengthWord(text);
        final List<String> all = SentenceRegex.getAllSentence(text);
        for (int i = 0; i < list.size(); i++) {
            logger.info(all.get(i));
            logger.info(list.get(i));
        }
    }

    private List<String> shiftVowelMaxLengthWord(String text) {
        return SentenceRegex.getAllSentence(text).stream()
                .map(this::shift)
                .collect(Collectors.toList());
    }

    private String shift(String string) {
        String vowelWord = WordRegex.vowel(string).findFirst().orElse("absent");
        String maxLength = SentenceRegex.getWordMaxLength(string).orElse("absent");
        if (string.indexOf(vowelWord) < string.indexOf(maxLength)) {
            string = string.replaceFirst(maxLength, vowelWord)
                    .replaceFirst(vowelWord, maxLength);
        } else {
            string = string.replaceFirst(vowelWord, maxLength)
                    .replaceFirst(maxLength, vowelWord);
        }
        return string;
    }
}
