package com.zaluskyi.controller;

import com.zaluskyi.model.Text;
import com.zaluskyi.model.entity.Word;
import com.zaluskyi.model.regex.LetterRegex;
import com.zaluskyi.model.regex.WordRegex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Task7 implements Controller {
    private Logger logger = LogManager.getLogger(Task7.class);

    public void run() {
        String text = Text.getReformedText("textfile.txt").orElse("file not exist or empty");
        ratioVowelToTotal(text).forEach(w -> logger.info(w.getWord() + " " + w.getQuantityDouble()));
    }

    private List<Word> ratioVowelToTotal(String string) {
        return WordRegex.stringStream(string)
                .map(word -> new Word(word, WordRegex.stringStream(word)
                        .mapToDouble(wordToChar -> wordToChar.codePoints()
                                .mapToObj(ch -> (char) ch)
                                .filter(LetterRegex::isVowel)
                                .count() / (double) wordToChar.length())
                        .findFirst()
                        .orElse(0)))
                .sorted(Comparator.comparing(Word::getQuantityDouble))
                .collect(Collectors.toList());
    }
}
