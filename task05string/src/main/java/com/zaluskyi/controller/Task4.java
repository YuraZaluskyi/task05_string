package com.zaluskyi.controller;

import com.zaluskyi.model.Text;
import com.zaluskyi.model.regex.SentenceRegex;
import com.zaluskyi.model.regex.WordRegex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;

public class Task4 implements Controller {
    private static Logger logger = LogManager.getLogger(Task4.class);

    public void run() {
        String text = Text.getReformedText("textfile.txt").orElse("file not exist or empty");
        specificWordInQuestionSentence(text, 5).forEach(logger::info);
    }

    private List<String> specificWordInQuestionSentence(String string, final int length) {
        return SentenceRegex.getQuestion(string).stream()
                .flatMap(WordRegex::specificStream)
                .map(String::toLowerCase)
                .distinct()
                .filter(word -> word.length() == length)
                .collect(Collectors.toList());
    }
}
