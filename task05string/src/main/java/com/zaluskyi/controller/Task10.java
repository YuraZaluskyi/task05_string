package com.zaluskyi.controller;

import com.zaluskyi.model.Text;
import com.zaluskyi.model.entity.Sentence;
import com.zaluskyi.model.regex.SentenceRegex;
import com.zaluskyi.model.regex.WordRegex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Task10 implements Controller {
    private static Logger logger = LogManager.getLogger(Task10.class);

    public void run() {
        String text = Text.getReformedText("textfile.txt").orElse("file not exist or empty");
        quantityWordInSentence(text, Arrays.asList("amy", "and"))
                .forEach(e -> logger.info(e.getSentence() + " " + e.getWord() + "=" + e.getQuantityWord()));
    }

    private List<Sentence> quantityWordInSentence(String text, List<String> words) {
        return SentenceRegex.getAllSentence(text).stream()
                .flatMap(sentence -> words.stream()
                        .map(word -> new Sentence(sentence, word, WordRegex.stringStream(sentence)
                                .filter(wordFromSentence -> wordFromSentence.equalsIgnoreCase(word))
                                .count())))
                .sorted(Comparator.comparing(Sentence::getQuantityWord, Collections.reverseOrder()))
                .collect(Collectors.toList());
    }
}
