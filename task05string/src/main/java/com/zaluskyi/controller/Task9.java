package com.zaluskyi.controller;

import com.zaluskyi.model.Text;
import com.zaluskyi.model.regex.LetterRegex;
import com.zaluskyi.model.regex.WordRegex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Task9 implements Controller {
    private static Logger logger = LogManager.getLogger(Task9.class);

    public void run() {
        String text = Text.getReformedText("textfile.txt").orElse("file not exist or empty");
        sortByCharQuantity(text, 'e').forEach(logger::info);
    }

    private List<String> sortByCharQuantity(String string, final char ch) {
        return WordRegex.stringStream(string)
                .map(String::toLowerCase)
                .sorted()
                .sorted(Comparator.comparing(w -> LetterRegex.find(w, ch).size(), Collections.reverseOrder()))
                .collect(Collectors.toList());
    }
}
