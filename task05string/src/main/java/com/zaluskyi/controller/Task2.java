package com.zaluskyi.controller;

import com.zaluskyi.model.Text;
import com.zaluskyi.model.regex.SentenceRegex;
import com.zaluskyi.model.regex.WordRegex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Task2 implements Controller {
    private static Logger logger = LogManager.getLogger(Task2.class);

    @Override
    public void run() {
        String text = Text.getReformedText("textfile.txt").orElse("file not exist or empty");
        sortByReversValue(quantityWordInSentence(text)).forEach((k, v) -> logger.info(k + " " + v));
    }

    private Map<String, Long> quantityWordInSentence(String strings) {
        return SentenceRegex.getAllSentence(strings).stream()
                .distinct()
                .collect(Collectors.toMap(k -> k, v -> WordRegex.stringStream(v).count()));
    }

    private LinkedHashMap<String, Long> sortByReversValue(Map<String, Long> map) {
        return map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }
}
