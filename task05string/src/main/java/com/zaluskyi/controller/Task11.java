package com.zaluskyi.controller;

import com.zaluskyi.model.Text;
import com.zaluskyi.model.regex.SentenceRegex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;

public class Task11 implements Controller {
    private static Logger logger = LogManager.getLogger(Task11.class);

    public void run() {
        String text = Text.getReformedText("textfile.txt").orElse("file not exist or empty");
        removePartSentence(text, 't', 'e').forEach(logger::info);
    }

    private List<String> removePartSentence(String string, final char start, final char end) {
        return SentenceRegex.getAllSentence(string).stream()
                .map(sentence -> sentence.replace(SentenceRegex.findMaxPartSentence(sentence, start, end).stream()
                        .findFirst()
                        .orElse(""), ""))
                .collect(Collectors.toList());
    }
}
