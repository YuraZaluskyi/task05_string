package com.zaluskyi.controller;

public interface Controller {
    void run();
}
