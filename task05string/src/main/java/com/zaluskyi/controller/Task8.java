package com.zaluskyi.controller;

import com.zaluskyi.model.Text;
import com.zaluskyi.model.regex.LetterRegex;
import com.zaluskyi.model.regex.WordRegex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Task8 implements Controller {
    private static Logger logger = LogManager.getLogger(Task8.class);

    public void run() {
        String text = Text.getReformedText("textfile.txt").orElse("file not exist or empty");
        sortByFirstConsonant(text).forEach(logger::info);
    }

    private List<String> sortByFirstConsonant(String string) {
        return WordRegex.stringStream(string)
                .filter(word -> LetterRegex.isVowel(word.charAt(0)))
                .filter(LetterRegex::isConsonant)
                .map(String::toLowerCase)
                .sorted(Comparator.comparing(o -> LetterRegex.getConsonant(o).stream()
                        .findFirst()
                        .orElse("")))
                .collect(Collectors.toList());
    }


}
