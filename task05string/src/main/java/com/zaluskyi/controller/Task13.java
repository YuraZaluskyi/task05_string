package com.zaluskyi.controller;

import com.zaluskyi.model.Text;
import com.zaluskyi.model.entity.Word;
import com.zaluskyi.model.regex.WordRegex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Task13 implements Controller {
    private static Logger logger = LogManager.getLogger(Task13.class);

    public void run() {
        String text = Text.getReformedText("textfile.txt").orElse("file not exist or empty");
        countLaterAndSort(text, 'a').forEach(logger::info);
    }

    private List<Word> countLaterAndSort(String string, final char letter) {
        return WordRegex.stringStream(string)
                .map(String::toLowerCase)
                .map(word -> new Word(word, WordRegex.stringStream(word)
                        .mapToLong(wordToChar -> wordToChar.codePoints()
                                .filter(ch -> ch == letter)
                                .count())
                        .findFirst()
                        .orElse(0)))
                .sorted(Comparator.comparing(Word::getQuantity, Comparator.reverseOrder())
                        .thenComparing(Word::getWord))
                .collect(Collectors.toList());
    }
}
