package com.zaluskyi.controller;

import com.zaluskyi.model.entity.Command;
import com.zaluskyi.model.entity.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class MainMenuController {
    private static Logger logger = LogManager.getLogger(MainMenuController.class);
    private Scanner input = new Scanner(System.in);
    private ArrayList<Command> commands = new ArrayList<>();
    private List<Menu> menuLocal = new ArrayList<>();
    private ResourceBundle resourceBundle;

    public void start() {
        menuLocal = makeMenuLocal();
        mainMenu();
    }

    private ArrayList<Menu> makeMenuLocal() {
        ArrayList<Menu> menu = new ArrayList<>();
        menu.add(new Menu("USA", new Locale("en", "US")));
        menu.add(new Menu("Українська", new Locale("uk", "UA")));
        menu.add(new Menu("Japanese", new Locale("ja", "JA")));
        return menu;
    }

    public void mainMenu() {
        for (int i = 0; i < menuLocal.size(); i++) {
            logger.trace(i + 1 + " " + menuLocal.get(i).getCommandTitle());
        }
        int index = scanner();
        if (index >= menuLocal.size()) {
            mainMenu();
        }
        resourceBundle = ResourceBundle.getBundle("messages", menuLocal.get(index).getLocale());
        commands = makeCommand(resourceBundle);
        while (true) {
            commandMenu();
        }
    }

    private void commandMenu() {
        logger.trace("0 " + resourceBundle.getString("command.0"));
        for (int i = 0; i < commands.size(); i++) {
            logger.trace(i + 1 + " " + commands.get(i).getTitle());
        }
        int index = scanner();
        if (index == -1) {
            mainMenu();
        } else if (index >= commands.size()) {
            return;
        } else {
            commands.get(index).getController().run();
        }
    }

    private ArrayList<Command> makeCommand(ResourceBundle messages) {
        ArrayList<Command> commandHolders = new ArrayList<>();
        commandHolders.add(new Command(messages.getString("command.1"), new Task1()));
        commandHolders.add(new Command(messages.getString("command.2"), new Task2()));
        commandHolders.add(new Command(messages.getString("command.3"), new Task3()));
        commandHolders.add(new Command(messages.getString("command.4"), new Task4()));
        commandHolders.add(new Command(messages.getString("command.5"), new Task5()));
        commandHolders.add(new Command(messages.getString("command.6"), new Task6()));
        commandHolders.add(new Command(messages.getString("command.7"), new Task7()));
        commandHolders.add(new Command(messages.getString("command.8"), new Task8()));
        commandHolders.add(new Command(messages.getString("command.9"), new Task9()));
        commandHolders.add(new Command(messages.getString("command.10"), new Task10()));
        commandHolders.add(new Command(messages.getString("command.11"), new Task11()));
        commandHolders.add(new Command(messages.getString("command.12"), new Task12()));
        commandHolders.add(new Command(messages.getString("command.13"), new Task13()));
        return commandHolders;
    }

    private int scanner() {
        int index = Integer.MAX_VALUE;
        try {
            input = new Scanner(System.in);
            index = input.nextInt() - 1;
        } catch (NoSuchElementException e) {
            logger.error(e);
        }
        return index;
    }


}
