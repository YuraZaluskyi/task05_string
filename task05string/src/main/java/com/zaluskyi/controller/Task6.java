package com.zaluskyi.controller;

import com.zaluskyi.model.Text;
import com.zaluskyi.model.regex.WordRegex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;

public class Task6 implements Controller {
    private static Logger logger = LogManager.getLogger(Task6.class);
    private static char signTemp;

    public void run() {
        String text = Text.getReformedText("textfile.txt").orElse("file not exist or empty");
        newLetterNewLine(text).forEach(logger::info);
    }

    private List<String> newLetterNewLine(String string) {
        return WordRegex.stringStream(string)
                .map(String::toLowerCase)
                .sorted()
                .map(this::signController)
                .collect(Collectors.toList());
    }

    private String signController(String word) {
        if (signTemp != word.charAt(0)) {
            signTemp = word.charAt(0);
            word = "\t" + word;
        }
        return word + " ";
    }
}
