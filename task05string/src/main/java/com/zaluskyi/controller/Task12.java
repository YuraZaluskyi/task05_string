package com.zaluskyi.controller;

import com.zaluskyi.model.Text;
import com.zaluskyi.model.regex.LetterRegex;
import com.zaluskyi.model.regex.SentenceRegex;
import com.zaluskyi.model.regex.WordRegex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;

public class Task12 implements Controller {
    private static Logger logger = LogManager.getLogger(Task12.class);


    public void run() {
        String text = Text.getReformedText("textfile.txt").orElse("file not exist or empty");
        logger.info(deleteAllWords(text, findWortToRemove(text, 8)));
    }

    private List<String> findWortToRemove(String string, final int length) {
        return SentenceRegex.getAllSentence(string).stream()
                .flatMap(sentence -> WordRegex.stringStream(sentence)
                        .filter(w -> (w.length() == length)
                                && (LetterRegex.isConsonant(String.valueOf(w.charAt(0))))))
                .distinct()
                .collect(Collectors.toList());
    }

    private String deleteAllWords(String string, List<String> words) {
        String anotherText = string;
        for (String word : words) {
            anotherText = anotherText.replaceAll(word, "");
        }
        return anotherText;
    }
}
